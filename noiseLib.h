/*
*  All functions in this document are taken and/or modified from:
*	Apodaca, A., and Gritz, L., 2000. Advanced Renderman: 
*	Creating CGI for Motion Pictures. London: Academic Press.
*/

#define vsnoise(x) (2*(vector noise(x))-1)
#define snoise(x) (2*noise(x)-1)

//Voronoi Cell funtion
//modified from 'Advanced Renderman'
void voronoi(point P;
	     	float jitter;
	     	output float f1; output point pos1;
	     	output float f2; output point pos2;)
{
	point thisCell = point(floor(xcomp(P))+0.5, floor(ycomp(P))+0.5, floor(zcomp(P))+0.5);
	f1 = f2 = 1000;
	uniform float i, j, k;
	for(i=-1;i<=1;i+=1){
		for(j=-1;j<=1;j+=1){
			for(k=-1;k<=1;k+=1){
				point testCell = thisCell + vector(i,j,k);
				point pos = testCell + jitter * (vector cellnoise(testCell)-0.5);
				vector offset = pos - P;
				float dist = offset . offset; 
				if(dist < f1) {
					f2 = f1; pos2 = pos1;
					f1 = dist; pos1 = pos;
				}else if(dist < f2) {
					f2 = dist; pos2 = pos;
				}
			}
		}
	}
	f1 = sqrt(f1); f2 = sqrt(f2);
}

// Voronoi cell noise (a.k.a. Worley noise) -- 3-D, 1-feature version.
void voronoi_f1_3d (point P;
					float jitter;
					output float f1;
					output point pos1;
					)
{
 point thiscell = point (floor(xcomp(P))+0.5, floor(ycomp(P))+0.5,
  floor(zcomp(P))+0.5);
  f1 = 1000;
  uniform float i, j, k;
	for (i = -1; i <= 1; i += 1) {
		for (j = -1; j <= 1; j += 1) {
		  for (k = -1; k <= 1; k += 1) {
				point testcell = thiscell + vector(i,j,k);
				point pos = testcell + jitter * (vector cellnoise (testcell) - 0.5);
				vector offset = pos - P;
				float dist = offset . offset; // actually distˆ2
				if (dist < f1) {
          f1 = dist; pos1 = pos;
        }
      }
    }
  }
  f1 = sqrt(f1);
}


//Fractional Brownian Motion returning a vector
//modified from 'Advanced Renderman'
vector vfBm(point p; uniform float octaves, lacunarity, gain)
{
	uniform float amp = 1;
	varying point pp = p;
	varying vector sum = 0;
	uniform float i;

	for(i=0; i<octaves; i+=1) {
		sum += amp * vsnoise(pp); //defined above
		amp *= gain; pp *= lacunarity;
	}
	return sum;
}


//Fractional Brownian Motion returning a vector
//modified from 'Advanced Renderman'
float fBm(point p; uniform float octaves, lacunarity, gain)
{
	uniform float amp = 1;
	varying point pp = p;
	varying float sum = 0;
	uniform float i;

	for(i=0; i<octaves; i+=1) {
		sum += amp * snoise(pp); //defined above
		amp *= gain; pp *= lacunarity;
	}
	return sum;
}


//Turbulence Function
//modified from 'Advanced Renderman'
float turbulence (point P; uniform float octaves, lacunarity, gain)
{
	varying float sum = 0, amp =1;
	varying point pp = P;
	uniform float i;
	
	for(i=0; i<octaves; i+=1) {
		sum += amp * abs (snoise(pp));
		amp *= gain;
		pp *= lacunarity;
	}
	return sum;
}


color modNoise
(
 float RepeatS; float RepeatT;
 float Frequency;
 vector NoiseVec;
 )

 {
 normal Nf = faceforward(normalize(N),I);
 vector V = -normalize(I);

 point PP = transform("shader",P);
 normalize(NoiseVec);

 float ss=s+ float noise(PP*RepeatS)*Frequency;
 float tt=t+ float noise((PP*RepeatT) + NoiseVec)*Frequency;

 color Ct= cellnoise(ss*Frequency,tt*Frequency);

 return Ct;
}
