#include "noiseLib.h"

float getColour(vector _sunRay; float _lichenSpread; float _lichenSize)
{
	float colValue = 0;
	float amb = 1;
	float dif = 0.95;
	float spec = 0.005;
	float roughness = 0.1;
	float Scale = 0.76;
	float Layers = 21;
	
	float StartFreq=_lichenSize; 
	float gain=0.86; 
	float lacunarity=1.74;
	float Repeat=1.0; string noiseSpace="shader";
	color specularcolor=1; float ColourLayers=4;
	color Colour1= color "rgb" (0 ,0, 0);
	color Colour2= color "rgb" (0.2 ,0.2, 0.2);
	color Colour3= color "rgb" (0.35 ,0.35, 0.35);
	float rDiff=0.075; float gDiff=0.075; float bDiff=0.075;

	
	//Testing the dot product from the sun ray
  	float lichenValue = normalize(_sunRay).normalize(N);
	

	// init the shader values
	normal Nf = faceforward(normalize(N),I);
	vector V = -normalize(I);
	// here we do the texturing
	float i; float mag=0; float freq=StartFreq;
	point PP=transform(noiseSpace,P);
	PP*=Scale;
	for(i=0; i<Layers; i+=1)
	{
	mag+=abs(float noise(P*Repeat*freq)-0.5)*2/freq;
	freq*=lacunarity;
	}
	mag=smoothstep(0,_lichenSpread,mag);
	color MColours[];
	push(MColours,Colour1);
	float rC=rDiff; float gC=gDiff; float bC=bDiff;
	// build up an array of colours for the spline function
	color Ca;
	Ca=color "rgb"(Colour1[0]-rC,Colour1[1]-gC,Colour1[2]-bC);
	for (i=0; i<ColourLayers; i+=1)
	{
	push(MColours,Ca);
	Ca=color "rgb"(Ca[0]-rC,Ca[1]-gC,Ca[2]-bC);
	}
	Ca=color "rgb"(Colour2[0]-rDiff,Colour2[1]-gDiff,Colour2[2]-bDiff);
	for (i=0; i<ColourLayers; i+=1)
	{
	push(MColours,Ca);
	Ca=color "rgb"(Ca[0]-rC,Ca[1]-gC,Ca[2]-bC);
	}
	Ca=color "rgb"(Colour3[0]-rDiff,Colour3[1]-gDiff,Colour3[2]-bDiff);
	for (i=0; i<ColourLayers; i+=1)
	{
	push(MColours,Ca);
	Ca=color "rgb"(Ca[0]-rC,Ca[1]-gC,Ca[2]-bC);
	}

	if((lichenValue >= -1)&&(lichenValue < 0))
		mag *= (lichenValue*-0.1); //multiply mag by this and in the surface shader

	color Ct=spline(mag,MColours);

	colValue = Ct[0]+Ct[1]+Ct[2];
	return colValue;
}



/******************************
 The Rock discplacement shader 
 ******************************/
displacement RockDisp
(

 float Km=0.1;
 float Layers=8;
 vector SunRay=(0.2,0.6,-0.2);		// The direction of the sun (not light sourse / controls lichen position)
 float roughness = 0.2;				// Rock Surface & lichen roughness
 float shapeControl = 1.0;			// [-1, +1] from bloating to shrinking; 0 - no displacement
 float lichenScale = 0.5;			// Control for the scale of the lichen displacement
 float crackMag=0.5; 				// Voronoi Crack Maginitude (depth)
 float CrackWidth=1;				// The width of the cracks
 float lichenSpread=0.4;		 	// How much of the rock is covered in lichen
 float lichenSize=0.5;				// Individual lichen avg size on the rock surf
)
{
	// Modifying user input to conform to shader standards
	float lichenSpreadMod = (1.0-lichenSpread);
	float lichenSizeMod = (1.0-lichenSize)*4.0;

	/// Calculating a fake colour value using the same function ///
	/// which will define the colours in the surface shader ///
	/// Scaling down the displacement and clamping it to zero ///
  	float colValue=getColour(SunRay, lichenSpreadMod, lichenSizeMod);
  	colValue *= 0.01; 
	colValue = max(colValue, 0);
  

	/// Adding overall rock roughness accross the whole shape with a turbulence func
	float turb = turbulence(P, 5, 8, 1.5);
	P = P+(turb*(colValue*(roughness*roughness)+0.001))*normalize(N); 


	/// fBm displacement for the whole rock shape ///
	vector NN=normalize(N);
	float mag=0;
	float Freq=1;
	float octaves=8;
	float gain=2.0;
	point Pt=transform("shader",P);
	float i;
	for(i=0; i<octaves; i+=1)
	{
		mag+=(float noise(Pt*Freq)-0.5)*2/Freq;
		Freq*=gain;
	}
	mag /=length(vtransform("object",NN));
	P=P+mag*NN*Km*shapeControl;


	/// Displace Moss and Lichen based on the displacement fake 'colour value' ///
	float lichenMag = 2.0*(lichenScale); 
	NN=normalize(N); 
	point PP; 
  	PP=colValue*NN*lichenMag;
  	P=P+PP;
  	


  	/// Voronoi Cracks displacement ///
	float o_f1, o_f2;
	point o_pos1=0; point o_pos2=0; //initializing the positions to 0?
	mag=crackMag*-0.01;
	mag /= length(vtransform("object",NN));
	PP = transform("shader",P);
	PP = point (P + 0.1 * vfBm(2*P, 4, 2, 0.5)); //applying brownian motion to the cell borders
  	voronoi(PP, 1, o_f1, o_pos1, o_f2, o_pos2); //making a call to our voronoi cell func
  	float crackWidth = (distance(o_pos1,o_pos2) / (distance(o_pos1,P)+distance(P,o_pos2)))*CrackWidth;
  	color crackCol = smoothstep(o_f2-o_f1+0.001,o_f2-o_f1-0.001, 0.005*crackWidth);
	float crackDepth = (crackCol[0]+crackCol[1]+crackCol[2]);
  	NN=normalize(N);
  	P=P+mag*crackDepth*NN;
  	

	N=calculatenormal(P);
}

