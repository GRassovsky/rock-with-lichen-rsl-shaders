#include "noiseLib.h"

//OrenNayar BRDF taken from 'Advanced Renderman'
color LocIllumOrenNayar (normal N; vector V; float roughness;)
{
 // Surface roughness coefficients for Oren/Nayar's formula
 float sigma2 = roughness * roughness;
 float A = 1 - 0.5 * sigma2 / (sigma2 + 0.33);
 float B = 0.45 * sigma2 / (sigma2 + 0.09);
 // Useful precomputed quantities
 float theta_r = acos (V . N); // Angle between V and N
 vector V_perp_N = normalize(V-N*(V.N)); // Part of V perpendicular to N

 // Accumulate incoming radiance from lights in C
 color C = 0;
 extern point P;
 illuminance (P, N, PI/2)
 {
   vector LN = normalize(L);
   float cos_theta_i = LN . N;
   float cos_phi_diff = V_perp_N . normalize(LN - N*cos_theta_i);
   float theta_i = acos (cos_theta_i);
   float alpha = max (theta_i, theta_r);
   float beta = min (theta_i, theta_r);
   C += 1 * Cl * cos_theta_i * (A + B * max(0,cos_phi_diff) * sin(alpha) * tan(beta));
 }
 return C;
}


/*The Rock surface shader*/
surface RockSurf
(
 float Ka=1; float Kd=0.95; float Ks=0.005; float roughness = 0.1;  float Kr = 0.03;
 float Scale=0.76; float Layers=21;
 output varying float StartFreq=2.8; float gain=0.86; // mod startFreq - decreases the amount of lichen [1.5 - 5]
 float lacunarity=1.74; 											
 float Repeat=1.0; string noiseSpace="shader";
 color specularcolor=1; float ColourLayers=4;
 color C1= color "rgb" (0.60 ,0.60, 0.60);
 color C2= color "rgb" (0.82,0.82,0.82);
 color C3= color "rgb" (0.67 ,0.75, 0.01);
 float redDiff=0.075; float greenDiff=0.075; float blueDiff=0.075;
 float glitter = 0.15;
)
{
  // Modifying user input to conform to shader standards 
  color Colour1 = C1;
  color Colour2 = C2;
  color Colour3 = C3;
  float rDiff = redDiff;
  float gDiff = greenDiff;
  float bDiff = blueDiff;
  float moss = 0;

  float startFreqMod;
  if(displacement("lichenSize", startFreqMod) == 1)
    startFreqMod = (1.0-startFreqMod)*4.0;
  else
    startFreqMod = 2.8;


  //Taking in the SunRay parameter from the displacement shader
  // and checking if it exist, else giving a default ray
  vector SunRay;
  if(displacement("SunRay", SunRay) == 1)
  {}
  else
    SunRay=(0.2,0.6,-0.2); // default ray
  
  float lichenValue = normalize(SunRay).normalize(N);




  // Affecting the lichen by the sun ray attenuation of the dot product
  // Having more golden lichen were it is most sunny
  float lerp = (lichenValue*0.5)+0.5;
  Colour2 = color mix(Colour2, Colour3, pow(lerp, 20));


  float lichenSpread = 0;
  if(displacement("lichenSpread", lichenSpread) == 1)
    lichenSpread = (1.0-lichenSpread);


  // init the shader values
  normal Nf = faceforward(normalize(N),I);
  vector V = -normalize(I);
  float i; float mag=0; float freq=startFreqMod;
  point PP=transform(noiseSpace,P);
  PP*=Scale;
  for(i=0; i<Layers; i+=1)
  {
   mag+=abs(float noise(P*Repeat*freq)-0.5)*2/freq;
   freq*=lacunarity;
  }
  mag=smoothstep(0,lichenSpread,mag);
  color MColours[];
  push(MColours,Colour1);
  float rC=rDiff; float gC=gDiff; float bC=bDiff;
  // build up an array of colours for the spline function
  color Ca;
  Ca=color "rgb"(Colour1[0]-rC,Colour1[1]-gC,Colour1[2]-bC);
  for (i=0; i<ColourLayers; i+=1)
  {
   push(MColours,Ca);
   Ca=color "rgb"(Ca[0]-rC,Ca[1]-gC,Ca[2]-bC);
  }
  Ca=color "rgb"(Colour2[0]-rDiff,Colour2[1]-gDiff,Colour2[2]-bDiff);
  for (i=0; i<ColourLayers; i+=1)
  {
   push(MColours,Ca);
   Ca=color "rgb"(Ca[0]-rC,Ca[1]-gC,Ca[2]-bC);
  }
  Ca=color "rgb"(Colour3[0]-rDiff,Colour3[1]-gDiff,Colour3[2]-bDiff);
  for (i=0; i<ColourLayers; i+=1)
  {
   push(MColours,Ca);
   Ca=color "rgb"(Ca[0]-rC,Ca[1]-gC,Ca[2]-bC);
  }

  if((lichenValue >= -1)&&(lichenValue < 0))
    mag *= (lichenValue*-0.1); 

  color Ct=spline(mag,MColours);
  


  
  //  Making super specular particles inside the rock
  float spec = 1;
  color specDots = 0;  
	specDots = modNoise(20, 20, 100, vector (1,0,0));
  // The super glossy specs are only visible at sharp angles and where there is no moss
  if(((normalize(I).normalize(N)) < -0.5)&&(moss < 0.5))
  {
    // We specify only the rock nuances so that we don't get gloss on the lychen
  	if((specDots[0]+specDots[1]+specDots[2] > 3.0-glitter) && ((Ct[0]+Ct[1]+Ct[2] < 1.35)&&(Ct[0]+Ct[1]+Ct[2] >= 1.1))) 
  		spec = 1000;
  	else
  		spec = 1;
	}
  
	// Adding specularity to the darker regions
  // Works well with the transitions from rock to lichen
  if(Ct[0]+Ct[1]+Ct[2] < 1.1)
	  spec = 20;
    

  // Adding an Environment map
  vector Rcurrent = reflect(I,Nf);
  vector Rworld = vtransform("world",Rcurrent);
  color Cr = color environment("envMap.tx",Rworld);
 

  Oi=Os;
  Ci = Oi * (Ct * (Ka * ambient() + Kd*diffuse(Nf)) +specularcolor * spec * Ks* LocIllumOrenNayar(Nf, V, 1) + (Kr*Cr));
}


